$( window ).ready(function() {

    $('form[name=contactformA]').validate();
    $('form[name=contactformB]').validate();
    $('form[name=contactformC]').validate();

		// On submit of FORM A
		$('form[name=contactformA]').submit(function(e) {



            e.preventDefault();

            var $form = $(this);
            if(! $form.valid()) return false;

            var formName = "contactformA";
            contactFormWorking(formName);
            sendMessage(formName);

		});
		// END FORM A

		// On submit of FORM B
		$('form[name=contactformB]').submit(function(e) {
            e.preventDefault();
            var $form = $(this);
            if(! $form.valid()) return false;
            var formName = "contactformB";
            contactFormWorking(formName);
            sendMessage(formName);

		});
		// END FORM B


        // On submit of FORM C
		$('form[name=contactformC]').submit(function(e) {
            e.preventDefault();
            var $form = $(this);
            if(! $form.valid()) return false;
            var formName = "contactformC";
            contactFormWorking(formName);
            sendMessage(formName);

		});
		// END FORM C

        jQuery('#contactbuttoncta').bind('click', function(e) {
            jQuery.fancybox.open({
                href: '#contactformA',
                helpers: {
                    overlay: {
                        locked: false
                    }
                }
            });

            return false;
        });

});  // END DOC READY FUNC

function sendMessage (formName) {

    $.post(directory_uri + '/contact_results.php', {

           		message_name: $('form[name='+ formName +'] #Name').val(),
            	message_company: $('form[name='+ formName +'] #Company').val(),
            	message_role: $('form[name='+ formName +'] #Role').val(),
            	message_tel: $('form[name='+ formName +'] #Tel').val(),
            	message_email: $('form[name='+ formName +'] #Email').val(),
                message_interest: $('form[name='+ formName +'] #Interest').val()

       		 }, function($response){

  					console.log($response);
            		if ($response == 'success') {

                        contactFormSuccess(formName);

            		} else {

                        contactFormError(formName);

            		}

        	}).fail(function() { //alert("error");
        });


}


function contactFormWorking (name) {

    $('#'+name).children().css('visibility','hidden');
    $('#'+name).addClass('workingPopup');

}

function contactFormSuccess (name) {
     $('#'+name).removeClass();
    var successMessageHTML = "<div id='messageSent'><h2 class='green-header'>Thanks for connecting with us!</h2><p>We'll be contacting you shortly.</p>";
     $('#'+name).html(successMessageHTML);
}

function contactFormError (name) {
    $('#'+name).removeClass();
    var errorMessageHTML = "<div id='messageNotSent'><h2 class='green-header'>Sorry there was a problem sending your message.</h2><p>Please try again.</p>";
    $('#'+name).html(errorMessageHTML);
}
