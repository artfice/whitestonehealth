/*
 * jQuery WidowFix Plugin
 * http://matthewlein.com/widowfix/
 * Copyright (c) 2010 Matthew Lein
 * Version: 1.3.2 (7/23/2011)
 * Dual licensed under the MIT and GPL licenses
 * Requires: jQuery v1.4 or later
 */

(function(a){jQuery.fn.widowFix=function(d){var c={letterLimit:null,prevLimit:null,linkFix:false,dashes:false};var b=a.extend(c,d);if(this.length){return this.each(function(){var i=a(this);var n;if(b.linkFix){var h=i.find("a:last");h.wrap("<var>");var e=a("var").html();n=h.contents()[0];h.contents().unwrap()}var f=a(this).html().split(" "),m=f.pop();if(f.length<=1){return}function k(){if(m===""){m=f.pop();k()}}k();if(b.dashes){var j=["-","–","—"];a.each(j,function(o,p){if(m.indexOf(p)>0){m='<span style="white-space:nowrap;">'+m+"</span>";return false}})}var l=f[f.length-1];if(b.linkFix){if(b.letterLimit!==null&&n.length>=b.letterLimit){i.find("var").each(function(){a(this).contents().replaceWith(e);a(this).contents().unwrap()});return}else{if(b.prevLimit!==null&&l.length>=b.prevLimit){i.find("var").each(function(){a(this).contents().replaceWith(e);a(this).contents().unwrap()});return}}}else{if(b.letterLimit!==null&&m.length>=b.letterLimit){return}else{if(b.prevLimit!==null&&l.length>=b.prevLimit){return}}}var g=f.join(" ")+"&nbsp;"+m;i.html(g);if(b.linkFix){i.find("var").each(function(){a(this).contents().replaceWith(e);a(this).contents().unwrap()})}})}}})(jQuery);

/* MAIN CODE */

$( document ).ready(function() {

	$('.wfix').widowFix();
	
	startupSlider();
	
	// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    START SLIDER CODE   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	
	$('.slider .slide').hide();

/*
	var waitAmount = 0; // extra wait amount set to none
	var waitDefault = 6000; // default wait amount between slides
*/

	var NAVhoverIntentConfig = {    
     	over: navButtonOver, // function = onMouseOver callback (REQUIRED)    
     	timeout: 10, // number = milliseconds delay before onMouseOut 
     	interval:10,   
     	out: navButtonOut // function = onMouseOut callback (REQUIRED)    
	};
		
		startupSlider(); // calls function that gets the whole ball rolling ...

		
		
		//fade in initial slide, setrounded rounded corner css for first and last buttons, set first slide to active class
		function startupSlider () {
			
			$('.slider .slide:first').addClass('active').fadeIn(200);
			$('.navButton:first').addClass('active');
	
			
		};
	
		//add .active class and fade in next slide and matching nav button div, either the next in order or set by the index variable
		function rotate(index) {
			
	     	/*
$('.slider .slide.active').removeClass('active').fadeOut(200, function() { 
	         	$('.slider .slide:eq(' + index + ')').addClass('active').fadeIn(200);
	     		});
*/
	     	$('.slider .slide.active').removeClass('active').hide();
	     		
	     	$('.slider .slide.active').promise().done(function( arg1 ) {
  				$('.slider .slide:eq(' + index + ')').addClass('active').show();
			});
	     	$('.navSlider .navButton.active').removeClass('active');
	     	$('.navSlider .navButton:eq(' + index + ')').addClass('active');
	     	
	   
	     	

		};
		
		//nav button click event, finds relative list, also starts up its own interval with added time
		$('.navSlider .navButton a').click(function() {  
			
			
			var skipIndex = $('.slider').prevAll().find('.navButton').length;
			var index = $(this).parent().index('.navButton') - skipIndex;
	    	rotate(index);
	    	/* clearInterval(myInterval); */
	   	 	/* waitAmount = 10000; */

	   	 	/*
myInterval = setInterval(function() {
	    		var $next = $('.slider .slide.active').next();
	    		if ($next.length == 0){
	        		$next = $('.slider .slide:first');
				}
	   			rotate($next.index());
	   		}, (waitAmount + waitDefault));
*/

	   	 	return false;
	   	 	
		});
		
		//navButton hover event, set background color by relative node in backgroundColor array
		$('.navButton a').hoverIntent(NAVhoverIntentConfig);

		function navButtonOver () {
		
		};
		
		function navButtonOut() {
		
		};

		
			//initial interval before any button is clicked
		/*
myInterval = setInterval(function() {
	    	var $next = $('.slider .slide.active').next();
	    	if ($next.length == 0){
	        $next = $('.slider .slide:first');
			}
	   		rotate($next.index());
	   		
		}, (waitAmount + waitDefault));
*/
	

});  // END DOC READY FUNC

// sets up function to be called only after resizing of window is complete, keeps it from firing a bunch of times. Is set up to accept a unique string just in case it ends up being used by more than one function.
/*
var waitForFinalEvent = (function () {
  var timers = {};
  return function (callback, ms, uniqueId) {
    if (!uniqueId) {
      uniqueId = "Don't call this twice without a uniqueId";
    }
    if (timers[uniqueId]) {
      clearTimeout (timers[uniqueId]);
    }
    timers[uniqueId] = setTimeout(callback, ms);
  };
})();
*/


