if (typeof console == "undefined") var console = { log: function() {} };

// We Get It
var slider;
var isTopLoginShowing = false;
var cursec = 'navtop';
var lastww = 0;
var numberOfVideos = 3;
var wgiaccordion;
var windowWidth = 0;  // For use with media queries
var selectBoxTabs;

function kickstartResize() {
	lastw = -1;
	lastww = 0;
	handleWindowResize(null);
}

function handleWindowResize(event) {
	var scale;
	var ww = jQuery('#flexslidercontainer').width();
	windowWidth = window.innerWidth ? window.innerWidth : $(window).width();

	if (ww != lastww) {
		lastww = ww;

		scale = ww/900;
		var slH = 456 * ww/900;

		jQuery('.success-slide').css('width', ww + 'px')
			.css('height', slH + 'px');

		if (ww > 767)
		{
			var fontSize = scale * 16;
			jQuery('.slides').css('font-size', fontSize + 'px');
		}
		else
		{
			scale = 0.5 + 0.5 * ww/767;
			jQuery('.video-cta-text').removeAttr('style');
			scale = ww/900;
			jQuery('.slides').css('font-size', scale * 16 + 'px');
		}

		if (typeof slH === 'undefined' || slH == 0) {
			lastww = -1;
			setTimeout('handleWindowResize(null)', 100);
			return;
		}

		jQuery('.slides').find('.success-text').each(function(index, el) {
			var h = jQuery(this).height();
			var slTop = (slH - h)/2;

			if (slTop == slH/2)
			{
				lastww = -1;
				setTimeout('handleWindowResize(null)', 100);
				return;
			}
			jQuery(this).css('top', slTop + 'px');
		});
	}

	// tabs
	ww = jQuery('.tabbed').width();
	scale = ww/1212;
	var tabFontScale = (scale > 1) ? 15 : 15 * scale;
	jQuery('.tabs-nav').css('font-size', tabFontScale + 'px');
	//var tabPadScale = (scale > 1) ? 1 : scale;
	//jQuery('.tabs-nav ul li a').css('padding', (17 * tabPadScale) + 'px' + (27 * tabPadScale) + 'px' + (4 * tabPadScale) + 'px');

	// Match tab heights

	var maxHeight = 0;

	jQuery('.tabs-content > div').each(function(index, el) {
		if (jQuery(this).outerHeight() > maxHeight) {
			maxHeight = jQuery(this).outerHeight();
		}
	});

	jQuery('.tabs-content').css('height', maxHeight + 'px');

	wgiaccordion.handleWindowResize();
	handleEngagementResize();

}

function setUpPlayButton() {
	var i;

	if (jQuery('html').hasClass('canvas'))
	{
		for (i = 1; i <= numberOfVideos; i++)
		{
			var c = document.getElementById("playbutton" + i);
			//G_vmlCanvasManager.initElement(c);
			var ctx=c.getContext("2d");
			ctx.beginPath();
			ctx.arc(42,42,42,0,2*Math.PI);
			ctx.fillStyle = '#6acbc9';
		    ctx.fill();

		    ctx.beginPath();
		    ctx.moveTo(34, 27);
		    ctx.lineTo(59, 43);
		    ctx.lineTo(34, 59);
		    ctx.fillStyle = '#ffffff';
		    ctx.fill();

		    if (i == 0)
		    {
		    	jQuery('.video-cta .video-cta-playbutton a, .video-cta .video-cta-link').bind('click', function (e) {
			    	e.preventDefault();
			    	jQuery('#video-trigger-0').trigger('click');
			    	return false;
			    });
		    }
		    else
		    {
		    	jQuery('#video-ss-' + i + ' a').bind('click', function(e) {
		    		e.preventDefault();
		    		var id = jQuery(this).closest('div.success-slide').attr('id');
		    		id = parseInt(id.replace('video-ss-', ''));
		    		jQuery('#video-trigger-' + id).trigger('click');
		    		return false;
		    	});
		    }
		}
	}
}

function initTabs() {

	jQuery('.tabs-nav').find('a').each(function(index, el) {
		jQuery(this).attr('id', 'tabnav' + index);
	});

	jQuery('.tabs-content > div').each(function(index, el) {
		jQuery(this).attr('id', 'tabcontent' + index);
	});

	jQuery('.tabs-nav a').bind('click', function(e) {
		e.preventDefault();

		jQuery(this).closest('ul').find('a').removeClass('selected');;
		jQuery('.tabs-content > div').hide();

		jQuery(this).addClass('selected');

		var val = parseInt(jQuery(this).attr('id').replace('tabnav', ''));
		var id = '#tabcontent' + val;

		jQuery(id).show();
		selectBoxTabs.setValue(val);

		return false;
	});

	var showing = (jQuery('.tabs-dropdown').is(':visible'));

	if (!showing) jQuery('.tabs-dropdown').show();

	selectBoxTabs = new SelectBox($('select').selectBox().change(function() {
		var val = $(this).val();
		var id = '#tabcontent' + val;
		jQuery('.tabs-content > div').hide();
		jQuery(id).show();

		jQuery('.tabs-nav a').removeClass('selected');
		jQuery('#tabnav' + val).addClass('selected');
	}));

	if (!showing) jQuery('.tabs-dropdown').attr('style', '');

	$('.tabs-content > div').hide();
	$('#tabcontent0').show();
}

var RBHAccordion = function(id) {

	var id = id;
	var _this = this;
	var _container = jQuery(id);
	var _lastww = 0;
	var _selectedBlade = 0;
	var _bladeNaturalWidth = 60;
	var _bladeWidth = 60;
	var _openw = 0;
	var _bladeCount = jQuery(id + ' > li').length;
	var _bladeT = []; // target width
	var _bladeC = []; // current width
	var _windowWidth = 0;
	var PHONE_MAX_WIDTH = 500;

	this.handleWindowResize = function() {
		var ww = _container.width();
		_windowWidth = window.innerWidth ? window.innerWidth : $(window).width();

		if (ww !== _lastww) {

			_lastww = ww;

			for (var i = 0; i < _bladeCount; i++)
			{
				if (i == _selectedBlade) {
					_bladeT[i] = 1;
					_bladeC[i] = 1;
				} else {
					_bladeT[i] = 0;
					_bladeC[i] = 0;
				}
			}

			if (_windowWidth > PHONE_MAX_WIDTH)
			{
				$(id).find('li').css('height', '');

				_scale = ww/1212;
				if (_scale > 1) _scale = 1;
				_bladeWidth = parseInt(_bladeNaturalWidth * _scale);

				jQuery(id + ' div.ia-trigger').css('width', _bladeWidth + 'px');

				var position = 0;

				jQuery(id).find('> li').each(function(index, el) {

					jQuery(this).css('left', position + 'px');

					if (index == _selectedBlade) {
						_openw = Math.ceil(ww - (_bladeCount) * _bladeWidth);
						jQuery(this).css('width', _openw + 'px');
						jQuery('#blade' + index + ' div.ia-content-outer').css('width', (_openw + _bladeWidth) + 'px');
						position += _openw;
					} else {
						jQuery(this).css('width', _bladeWidth + 'px');
						jQuery('#blade' + index + ' div.ia-content-outer').css('width', (_openw + _bladeWidth) + 'px');
						position += _bladeWidth;
					}
				});

				// Resize Content Boxes
				var cbL = parseInt(93 * _scale);
				var cbW = _openw + _bladeWidth - cbL - 21 * _scale;

				jQuery(id + ' div.ia-content-inner').css('width', cbW + 'px')
					.css('left', cbL + 'px');
			} // end desktop/ipad
			else
			{
				// mobile phones and tiny tablets
				jQuery(id).find('li, .ia-trigger').css('width', '');
				jQuery(id + ' div.ia-content-inner').css('width', '100%').css('left', '0px');
			}
		}
	}

	this.maintainBladeWidths = function() {
		var i;
		var acc = 0;

		for (i = 0; i < _bladeCount; i++) {
			_bladeC[i] = (_bladeT[i] + _bladeC[i] * 7)/8;
			if (_bladeC[i] < 0.01) _bladeC[i] = 0;
			if (_bladeC[i] > 0.99) _bladeC[i] = 1;
			acc += _bladeC[i];
		}

		if (_windowWidth > PHONE_MAX_WIDTH)
		{
			// iPad/desktop

			var room = _openw;
			var position = 0;

			for (i = 0; i < _bladeCount; i++) {
				var bw = Math.round(_bladeWidth + room * (_bladeC[i]/acc));
				jQuery(id + ' #blade' + i).css('width', bw + 'px')
					.css('left', position + 'px');
				position += bw;

				var cbL = parseInt(93 * _scale);
				var cbW = bw - cbL - 21 * _scale;

				jQuery('#blade' + i + ' div.ia-content-outer').css('width', bw + 'px')
					.css('height', '345px');
			}
		}
		else
		{
			// mobile phones

			for (i = 0; i < _bladeCount; i++) {

				jQuery('#blade' + i + ' div.ia-content-outer').css('width', '100%')
					.css('height', 'auto');

				var pct = _bladeC[i]/acc;
				var h = jQuery(id + ' #blade' + i + ' .ia-content-outer').height();
				var bw = 48 + Math.round(h * pct);

				jQuery(id + ' #blade' + i).css('height', bw + 'px')
					.css('left', '0px');


			}
		}


	}

	// Initialize

	for (var i = 0; i < _bladeCount; i++)
	{
		if (i == _selectedBlade) {
			_bladeT.push(1);
			_bladeC.push(1);
		} else {
			_bladeT.push(0);
			_bladeC.push(0);
		}
	}

	jQuery(id + ' div.ia-trigger').bind('click', function(e) {
		e.preventDefault();

		_selectedBlade = parseInt(jQuery(this).parent().attr('id').replace('blade', ''));

		for (var i = 0; i < _bladeCount; i++)
		{
			if (i == _selectedBlade) {
				_bladeT[i] = 1;
			} else {
				_bladeT[i] = 0;
			}
		}

		return false;
	});

	setInterval(function() {
		_this.maintainBladeWidths();
	}, 16);
}

// ENGAGEMENT

function initEngagement() {
	var i;

	for (i = 0; i < 4; i++)
	{
		// Copy contents from non-pop to pop
		jQuery('#pop-engage-' + i).html(jQuery('#engage-' + i).html());

		jQuery('#engagement-hs-' + i).mouseover(function(e) {
			var id = jQuery(this).attr('id').replace('engagement-hs-', '');
			jQuery('#engagementimage').attr('src', templatePath + '/images/wegetit/engagement-diagram-h' + id + '.png');

			if (windowWidth > 768) {
				showEngagementRollover(id);
			}
		}).mouseout(function(e) {
			jQuery('#engagementimage').attr('src', templatePath + '/images/wegetit/engagement-diagram.png');

			if (windowWidth > 768) {
				hideEngagementRollover();
			}
		}).bind('click', function(e) {
			e.preventDefault();

			var id = jQuery(this).attr('id').replace('engagement-hs-', '');

			if (windowWidth <= 768) {
				jQuery.fancybox.open({
	   				href: '#pop-engage-' + id,
	   				helpers: {
						overlay: {
							locked: false
						}
					}
	   			});
			}

			return false;
		})
	}
}

function showEngagementRollover(id) {
	jQuery('#engagementpopout').show();
	jQuery('#engagementpopout > div.popout').hide();
	jQuery('#engage-' + id).show();
}

function hideEngagementRollover() {
	jQuery('#engagementpopout').hide();
}

function handleEngagementResize() {

	var scale = jQuery('#engagementimage').width()/565;

	jQuery('#engagementpopout').css('left', '-' + (scale * 446) + 'px')
		.css('top', (scale * 199) + 'px')
		.css('width', (scale * 464) + 'px')
		.css('height', (scale * 194) + 'px')
		.css('font-size', (scale * 15) + 'px');
}

// INIT DOCUMENT JS

jQuery(document).ready(function() {
	jQuery('input.iradio').iCheck({
		labelHover: false,
		cursor: true,
		radioClass: 'iradio_flat-green'
	});

	// Flex Slider

	jQuery('.fancybox').fancybox({
        helpers: {
            media: {},
            overlay: {
            	locked: false
            }
        }
    });

	jQuery('.flexslider').flexslider({
		animation: "slide"
	});

	setUpPlayButton();

	setTimeout('kickstartResize()', 500);

	// Tabs

	initTabs();

	// Accordion

	wgiaccordion = new RBHAccordion('#info-accordion', 'wgiaccordion');
	wgiaccordion.handleWindowResize();

	// Contact

	jQuery('#contactbutton').bind('click', function(e) {
		e.preventDefault();

		var val = $('input[name=showyou]:checked', '#contactform').val();

		if (typeof val !== 'undefined')
		{
			console.log(val);

			jQuery.fancybox.open({
   				href: '#contactform' + val,
   				helpers: {
					overlay: {
						locked: false
					}
				}
   			});
		}

		return false;
	});

	// Engagement

	initEngagement();

	// Responsive Image Map
	jQuery('img[usemap]').rwdImageMaps();

	// Resize Handler

	jQuery(window).resize(function() {
		handleWindowResize();
	});

	handleWindowResize();
});
