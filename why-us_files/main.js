/* Placeholders.js v4.0.1 */
/*!
 * The MIT License
 *
 * Copyright (c) 2012 James Allardice
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
!function(a){"use strict";function b(){}function c(){try{return document.activeElement}catch(a){}}function d(a,b){for(var c=0,d=a.length;d>c;c++)if(a[c]===b)return!0;return!1}function e(a,b,c){return a.addEventListener?a.addEventListener(b,c,!1):a.attachEvent?a.attachEvent("on"+b,c):void 0}function f(a,b){var c;a.createTextRange?(c=a.createTextRange(),c.move("character",b),c.select()):a.selectionStart&&(a.focus(),a.setSelectionRange(b,b))}function g(a,b){try{return a.type=b,!0}catch(c){return!1}}function h(a,b){if(a&&a.getAttribute(B))b(a);else for(var c,d=a?a.getElementsByTagName("input"):N,e=a?a.getElementsByTagName("textarea"):O,f=d?d.length:0,g=e?e.length:0,h=f+g,i=0;h>i;i++)c=f>i?d[i]:e[i-f],b(c)}function i(a){h(a,k)}function j(a){h(a,l)}function k(a,b){var c=!!b&&a.value!==b,d=a.value===a.getAttribute(B);if((c||d)&&"true"===a.getAttribute(C)){a.removeAttribute(C),a.value=a.value.replace(a.getAttribute(B),""),a.className=a.className.replace(A,"");var e=a.getAttribute(I);parseInt(e,10)>=0&&(a.setAttribute("maxLength",e),a.removeAttribute(I));var f=a.getAttribute(D);return f&&(a.type=f),!0}return!1}function l(a){var b=a.getAttribute(B);if(""===a.value&&b){a.setAttribute(C,"true"),a.value=b,a.className+=" "+z;var c=a.getAttribute(I);c||(a.setAttribute(I,a.maxLength),a.removeAttribute("maxLength"));var d=a.getAttribute(D);return d?a.type="text":"password"===a.type&&g(a,"text")&&a.setAttribute(D,"password"),!0}return!1}function m(a){return function(){P&&a.value===a.getAttribute(B)&&"true"===a.getAttribute(C)?f(a,0):k(a)}}function n(a){return function(){l(a)}}function o(a){return function(){i(a)}}function p(a){return function(b){return v=a.value,"true"===a.getAttribute(C)&&v===a.getAttribute(B)&&d(x,b.keyCode)?(b.preventDefault&&b.preventDefault(),!1):void 0}}function q(a){return function(){k(a,v),""===a.value&&(a.blur(),f(a,0))}}function r(a){return function(){a===c()&&a.value===a.getAttribute(B)&&"true"===a.getAttribute(C)&&f(a,0)}}function s(a){var b=a.form;b&&"string"==typeof b&&(b=document.getElementById(b),b.getAttribute(E)||(e(b,"submit",o(b)),b.setAttribute(E,"true"))),e(a,"focus",m(a)),e(a,"blur",n(a)),P&&(e(a,"keydown",p(a)),e(a,"keyup",q(a)),e(a,"click",r(a))),a.setAttribute(F,"true"),a.setAttribute(B,T),(P||a!==c())&&l(a)}var t=document.createElement("input"),u=void 0!==t.placeholder;if(a.Placeholders={nativeSupport:u,disable:u?b:i,enable:u?b:j},!u){var v,w=["text","search","url","tel","email","password","number","textarea"],x=[27,33,34,35,36,37,38,39,40,8,46],y="#ccc",z="placeholdersjs",A=new RegExp("(?:^|\\s)"+z+"(?!\\S)"),B="data-placeholder-value",C="data-placeholder-active",D="data-placeholder-type",E="data-placeholder-submit",F="data-placeholder-bound",G="data-placeholder-focus",H="data-placeholder-live",I="data-placeholder-maxlength",J=100,K=document.getElementsByTagName("head")[0],L=document.documentElement,M=a.Placeholders,N=document.getElementsByTagName("input"),O=document.getElementsByTagName("textarea"),P="false"===L.getAttribute(G),Q="false"!==L.getAttribute(H),R=document.createElement("style");R.type="text/css";var S=document.createTextNode("."+z+" {color:"+y+";}");R.styleSheet?R.styleSheet.cssText=S.nodeValue:R.appendChild(S),K.insertBefore(R,K.firstChild);for(var T,U,V=0,W=N.length+O.length;W>V;V++)U=V<N.length?N[V]:O[V-N.length],T=U.attributes.placeholder,T&&(T=T.nodeValue,T&&d(w,U.type)&&s(U));var X=setInterval(function(){for(var a=0,b=N.length+O.length;b>a;a++)U=a<N.length?N[a]:O[a-N.length],T=U.attributes.placeholder,T?(T=T.nodeValue,T&&d(w,U.type)&&(U.getAttribute(F)||s(U),(T!==U.getAttribute(B)||"password"===U.type&&!U.getAttribute(D))&&("password"===U.type&&!U.getAttribute(D)&&g(U,"text")&&U.setAttribute(D,"password"),U.value===U.getAttribute(B)&&(U.value=T),U.setAttribute(B,T)))):U.getAttribute(C)&&(k(U),U.removeAttribute(B));Q||clearInterval(X)},J);e(a,"beforeunload",function(){M.disable()})}}(this),function(a,b){"use strict";var c=a.fn.val,d=a.fn.prop;b.Placeholders.nativeSupport||(a.fn.val=function(a){var b=c.apply(this,arguments),d=this.eq(0).data("placeholder-value");return void 0===a&&this.eq(0).data("placeholder-active")&&b===d?"":b},a.fn.prop=function(a,b){return void 0===b&&this.eq(0).data("placeholder-active")&&"value"===a?"":d.apply(this,arguments)})}(jQuery,this);

/* SLIDEOUT */
if (!jQuery('html').hasClass('ie8'))
{
	!function(t){if("object"==typeof exports&&"undefined"!=typeof module)module.exports=t();else if("function"==typeof define&&define.amd)define([],t);else{var e;"undefined"!=typeof window?e=window:"undefined"!=typeof global?e=global:"undefined"!=typeof self&&(e=self),e.Slideout=t()}}(function(){var t,e,n;return function i(t,e,n){function o(s,a){if(!e[s]){if(!t[s]){var u=typeof require=="function"&&require;if(!a&&u)return u(s,!0);if(r)return r(s,!0);var f=new Error("Cannot find module '"+s+"'");throw f.code="MODULE_NOT_FOUND",f}var l=e[s]={exports:{}};t[s][0].call(l.exports,function(e){var n=t[s][1][e];return o(n?n:e)},l,l.exports,i,t,e,n)}return e[s].exports}var r=typeof require=="function"&&require;for(var s=0;s<n.length;s++)o(n[s]);return o}({1:[function(t,e,n){"use strict";var i=t("decouple");var o=t("emitter");var r;var s=false;var a=window.document;var u=a.documentElement;var f=window.navigator.msPointerEnabled;var l={start:f?"MSPointerDown":"touchstart",move:f?"MSPointerMove":"touchmove",end:f?"MSPointerUp":"touchend"};var c=function _(){var t=/^(Webkit|Khtml|Moz|ms|O)(?=[A-Z])/;var e=a.getElementsByTagName("script")[0].style;for(var n in e){if(t.test(n)){return"-"+n.match(t)[0].toLowerCase()+"-"}}if("WebkitOpacity"in e){return"-webkit-"}if("KhtmlOpacity"in e){return"-khtml-"}return""}();function p(t,e){for(var n in e){if(e[n]){t[n]=e[n]}}return t}function h(t,e){t.prototype=p(t.prototype||{},e.prototype)}function d(t){t=t||{};this._startOffsetX=0;this._currentOffsetX=0;this._opening=false;this._moved=false;this._opened=false;this._preventOpen=false;this._touch=t.touch===undefined?true:t.touch&&true;this.panel=t.panel;this.menu=t.menu;this.panel.className+=" slideout-panel";this.menu.className+=" slideout-menu";this._fx=t.fx||"ease";this._duration=parseInt(t.duration,10)||300;this._tolerance=parseInt(t.tolerance,10)||70;this._padding=this._translateTo=parseInt(t.padding,10)||256;this._orientation=t.side==="right"?-1:1;this._translateTo*=this._orientation;if(this._touch){this._initTouchEvents()}}h(d,o);d.prototype.open=function(){var t=this;this.emit("beforeopen");if(u.className.search("slideout-open")===-1){u.className+=" slideout-open"}this._setTransition();this._translateXTo(this._translateTo);this._opened=true;setTimeout(function(){t.panel.style.transition=t.panel.style["-webkit-transition"]="";t.emit("open")},this._duration+50);return this};d.prototype.close=function(){var t=this;if(!this.isOpen()&&!this._opening){return this}this.emit("beforeclose");this._setTransition();this._translateXTo(0);this._opened=false;setTimeout(function(){u.className=u.className.replace(/ slideout-open/,"");t.panel.style.transition=t.panel.style["-webkit-transition"]=t.panel.style[c+"transform"]=t.panel.style.transform="";t.emit("close")},this._duration+50);return this};d.prototype.toggle=function(){return this.isOpen()?this.close():this.open()};d.prototype.isOpen=function(){return this._opened};d.prototype._translateXTo=function(t){this._currentOffsetX=t;this.panel.style[c+"transform"]=this.panel.style.transform="translate3d("+t+"px, 0, 0)"};d.prototype._setTransition=function(){this.panel.style[c+"transition"]=this.panel.style.transition=c+"transform "+this._duration+"ms "+this._fx};d.prototype._initTouchEvents=function(){var t=this;i(a,"scroll",function(){if(!t._moved){clearTimeout(r);s=true;r=setTimeout(function(){s=false},250)}});a.addEventListener(l.move,function(e){if(t._moved){e.preventDefault()}});this.panel.addEventListener(l.start,function(e){if(typeof e.touches==="undefined"){return}t._moved=false;t._opening=false;t._startOffsetX=e.touches[0].pageX;t._preventOpen=!t._touch||!t.isOpen()&&t.menu.clientWidth!==0});this.panel.addEventListener("touchcancel",function(){t._moved=false;t._opening=false});this.panel.addEventListener(l.end,function(){if(t._moved){t._opening&&Math.abs(t._currentOffsetX)>t._tolerance?t.open():t.close()}t._moved=false});this.panel.addEventListener(l.move,function(e){if(s||t._preventOpen||typeof e.touches==="undefined"){return}var n=e.touches[0].clientX-t._startOffsetX;var i=t._currentOffsetX=n;if(Math.abs(i)>t._padding){return}if(Math.abs(n)>20){t._opening=true;var o=n*t._orientation;if(t._opened&&o>0||!t._opened&&o<0){return}if(o<=0){i=n+t._padding*t._orientation;t._opening=false}if(!t._moved&&u.className.search("slideout-open")===-1){u.className+=" slideout-open"}t.panel.style[c+"transform"]=t.panel.style.transform="translate3d("+i+"px, 0, 0)";t.emit("translate",i);t._moved=true}})};d.prototype.enableTouch=function(){this._touch=true;return this};d.prototype.disableTouch=function(){this._touch=false;return this};e.exports=d},{decouple:2,emitter:3}],2:[function(t,e,n){"use strict";var i=function(){return window.requestAnimationFrame||window.webkitRequestAnimationFrame||function(t){window.setTimeout(t,1e3/60)}}();function o(t,e,n){var o,r=false;function s(t){o=t;a()}function a(){if(!r){i(u);r=true}}function u(){n.call(t,o);r=false}t.addEventListener(e,s,false)}e.exports=o},{}],3:[function(t,e,n){"use strict";var i=function(t,e){if(!(t instanceof e)){throw new TypeError("Cannot call a class as a function")}};n.__esModule=true;var o=function(){function t(){i(this,t)}t.prototype.on=function e(t,n){this._eventCollection=this._eventCollection||{};this._eventCollection[t]=this._eventCollection[t]||[];this._eventCollection[t].push(n);return this};t.prototype.once=function n(t,e){var n=this;function i(){n.off(t,i);e.apply(this,arguments)}i.listener=e;this.on(t,i);return this};t.prototype.off=function o(t,e){var n=undefined;if(!this._eventCollection||!(n=this._eventCollection[t])){return this}n.forEach(function(t,i){if(t===e||t.listener===e){n.splice(i,1)}});if(n.length===0){delete this._eventCollection[t]}return this};t.prototype.emit=function r(t){var e=this;for(var n=arguments.length,i=Array(n>1?n-1:0),o=1;o<n;o++){i[o-1]=arguments[o]}var r=undefined;if(!this._eventCollection||!(r=this._eventCollection[t])){return this}r=r.slice(0);r.forEach(function(t){return t.apply(e,i)});return this};return t}();n["default"]=o;e.exports=n["default"]},{}]},{},[1])(1)});
}

var contactFormHeight = 400;

function informReady() {
	var h = $('.fancybox-wrap iframe').contents().find('#contactformA').outerHeight();
	contactFormHeight = h;
}

(function($)
{
	var isMobileNavVisible = false;
	var lastW = 1000;
	var SWAP_SIZE = 916;
	var isMobile = false;
	var PHONE_WIDTH = 414;
	var hideSideNavInterval = -1;
	var isSlideoutVisibleIE9 = false;
	var TABLET_WIDTH = 1022;
	var isIE8 = (jQuery('html').hasClass('ie8'));

	function matchMatchers() {
		var i = 0;

		$('.matcher').css('height', 'auto');

		while ($('#match_' + i + '_0').length)
		{
			var j = 0;
			var mh = 0;

			while ($('#match_' + i + '_' + j).length) {
				
				if ($('#match_' + i + '_' + j).outerHeight()  > mh) {
					mh = $('#match_' + i + '_' + j).outerHeight();
				}

				j++;
			}

			for (m = 0; m < j; m++) {
				var pt = parseInt($('#match_' + i + '_' + m).css('padding-top').replace('px', ''));
				var pb = parseInt($('#match_' + i + '_' + m).css('padding-bottom').replace('px', ''));
				var th = mh - (pt + pb);
				$('#match_' + i + '_' + m).css('height', mh + 'px');
			}

			i++;
		}
	}

	function handleWindowResized() {
		var w = window.innerWidth ? window.innerWidth : $(window).width();
		var h = window.innerHeight ? window.innerHeight : $(window).height();
		var isToggled = false;
		var isLessThanSwap = false;

		// Fancy javascript for footer placement

		if (!$('html').hasClass('ie8'))
		{
			var fh = $('.footer').outerHeight();
			$('.push').css('height', fh + 'px');
			$('.wrapper').css('margin-bottom', '-' + fh + 'px');
		}

		lastW = w;
		isMobile = (w <= SWAP_SIZE);

		matchMatchers();
	}

	function checkHideSideNav() {

		var w = window.innerWidth ? window.innerWidth : $(window).width();

		if ($('html').hasClass('ie9') || $('html').hasClass('ie8'))
		{
			if (isSlideoutVisibleIE9) {
				$('#sidebar-menu').show();
			} else {
				$('#sidebar-menu').hide();
			}
		}
		else
		{
			if ($('#panel').css('transform') == '' || $('#panel').css('transform') == 'none') // || !(w <= 1022 && w > 414)
	    	{
	    		$('#sidebar-menu').hide();
	    	}
	    	else
	    	{
	    		$('#sidebar-menu').show();	
	    	}
		}
	}

	jQuery(document).ready(function() {

		//*******************************************************
		// Slideout navigation
		//*******************************************************

		if (!isIE8)
		{
			var slideout = new Slideout({
				'panel': document.getElementById('panel'),
				'menu': document.getElementById('sidebar-menu'),
				'padding': 256,
				'tolerance': 70,
				'side': 'right'
			});
		}

		$('#nav-toggle a').bind('click', function(e) {

			var w = window.innerWidth ? window.innerWidth : $(window).width();

			if (w <= PHONE_WIDTH)
			{
				if (isMobileNavVisible) 
				{
					$('.nav').stop().removeClass('opened').animate({
						height: 0
					}, 333);

					isMobileNavVisible = false;
				}
				else
				{
					var h = $('.nav').find('ul').first().height();

					$('.nav').stop().addClass('opened').animate({
						height: h
					}, 1000, function() {
						$('.nav').css('height', 'auto');
					});

					isMobileNavVisible = true;
				}
			}
			else
			{
				if ($('html').hasClass('ie9') || $('html').hasClass('ie8'))
				{
					if (!isSlideoutVisibleIE9) 
					{
						isSlideoutVisibleIE9 = true;

						$('#panel').stop().animate({
							left: -250
						}, 'fast');
					}
					else
					{
						isSlideoutVisibleIE9 = false;
						
						$('#panel').stop().animate({
							left: 0
						}, 'fast', function() {
							$('#panel').css('left', '');
						});
					}
				}
				else
				{
					if (typeof slideout !== 'undefined') slideout.toggle();
				}
			}
	        
	        return false;
	    });

		// Desktop dropdown

		$('.nav li.dropdown').mouseover(function(e) {
			if (!isMobile)
			{
				jQuery(this).addClass('hovered');
		   		var h = jQuery(this).find('div.dropdown > ul').first().height() + 20;
		   
		   		jQuery(this).find('div.dropdown').first().stop().animate({
		   			height: h
		   		}, 200);
			}

	   		return false;
		}).mouseout(function(e) {
			if (!isMobile)
			{
				jQuery(this).removeClass('hovered');
	   		
		   		jQuery(this).find('div.dropdown').stop().animate(
		   		{
		   			height: 0
		   		}, 100);
			}
	   		
	   		return false;
		});

		/* IPAD SUPPORT */

		if ($('html').hasClass('touch'))
		{
			$('.nav li.dropdown a.secondary-link').on('touchstart', function(e)
			{
				if ($(this).hasClass('touched')) 
				{
					$(document).off('click', handler);
					return true;
				}
				else
				{
					e.preventDefault();

					hideTouchNav();

					$(this).addClass('touched');

					$(this).parent().addClass('hovered');

					var h = $(this).parent().find('div.dropdown > ul').first().height();
			   
			   		$(this).parent().find('div.dropdown').first().stop().animate({
			   			height: h
			   		}, 200);

					$(document).on('touchstart click', handler);

					return false;
				}
			});

			/*
			$(document).click(function()
			{
				hideTouchNav();
			});

			$('.nav li.dropdown').click(function(e) {
				e.stopPropagation();
				return false;
			});
			*/
		}

		$('.nav .mobilearrow a, #sidebar-menu .mobilearrow a').bind('click', function(e) {

			if ($(this).hasClass('mobiletriggered'))
			{
				$(this).removeClass('mobiletriggered');
				var p = $(this).parent().parent();

				$(p).find('div.dropdown').first().stop().animate({
		   			height: 0
		   		}, 200);
			}
			else
			{
				$(this).addClass('mobiletriggered');

				var p = $(this).parent().parent();

				var h = $(p).find('div.dropdown > ul').first().height();

				$(p).find('div.dropdown').first().stop().animate({
		   			height: h
		   		}, 200);
			}

			return false;
		});

		handleWindowResized();

		jQuery(window).resize(function() {
			handleWindowResized();
		});

		hideSideNavInterval = setInterval(checkHideSideNav, 100);

		$('.contact-popup-button').bind('click', function(e) {

			jQuery.fancybox.open({
				href: templatePath + '/popup-contact-form.php',
				type: 'iframe',
				maxWidth: '420px',
				autoSize: false,
				helpers: {
					overlay: {
						locked: true
					}
				},
				afterLoad: function() {
					$('.fancybox-inner').css('height', contactFormHeight + 'px');
				}
			});

			return false;
		});
	});
})(jQuery);