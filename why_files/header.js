$(document).ready(function () {

	
	$(".video-container").fitVids();



	// <<<<<<<<<<<<<<<<<<< MEGA MENUS >>>>>>>>>>>>>>>>>>>>>

	
	 //add working js classes hide mega menus 
	$('.mega_menuHolder').addClass('working_js');
	$('.megaMenuStart').addClass('working_js');
	$('#mega_menu1Row').addClass('working_js');
	
	$('#megaMenu1').hide();
	$('#megaMenu2').hide();
	$('#megaMenu3').hide();
	$('#megaMenu4').hide();
	$('#megaMenu5').hide();
	
	
	//MEGA MENU DELAY VAR
	var mnuTimeout = null;	
	var $latestMenu = 0;
	
	//mega menus HEADER BUTTON hover functions
	$('.headerContainer').find('.headerBtn:eq(0)').hover(
    	function () {
    		clearDelay(); 	
    		$currentMenu = 1;
    		showMenu($currentMenu);
   		},function () {
   			mnuTimeout = setTimeout(function(){ hideAll() },200);
  		}
	);
	$('.headerContainer').find('.headerBtn:eq(1)').hover(
    	function () {
    		clearDelay(); 	
    		$currentMenu = 2;
    		showMenu($currentMenu);
   		},function () {
   			mnuTimeout = setTimeout(function(){ hideAll() },200);
  		}
	);
	$('.headerContainer').find('.headerBtn:eq(2)').hover(
    	function () {
    		clearDelay(); 	
    		$currentMenu = 3;
    		showMenu($currentMenu);
   		},function () {
   			mnuTimeout = setTimeout(function(){ hideAll() },200);
  		}
	);
	$('.headerContainer').find('.headerBtn:eq(3)').hover(
    	function () {
    		clearDelay(); 	
    		$currentMenu = 4;
    		showMenu($currentMenu);
   		},function () {
   			
   			mnuTimeout = setTimeout(function(){ hideAll() },200);
  		}
	);


	// Mega menu moust enter- and leave functions - reset timer and hold 
	$('.mega_menu').bind('mouseenter', clearDelay);
	$('.mega_menu').bind('mouseleave', hideAll);
	
	// clear and hold delay
	function clearDelay() {
		clearTimeout(mnuTimeout);	
	}

   // show current menu, hide others
	function showMenu($currentMenu) {
		
		var $showWhat = '#megaMenu'+$currentMenu;
		$($showWhat).show().fadeIn('fast');
		hideOthers($currentMenu);
		
	}
	
	//hide all but the current mega menu
	function hideOthers($currentMenu) {

		var $showWhat = '#megaMenu'+$currentMenu;
		myArray = new Array('#megaMenu1', '#megaMenu2', '#megaMenu3', '#megaMenu4', '#megaMenu5');
		
		$.each(myArray, function(index, value) {
			if (value == $showWhat) {
				//do nothing
			} else {
				$(value).stop().fadeOut('fast');
			}
		});
		
	}
	
	//hide all mega menus
	function hideAll($currentMenu) {
		$currentMenu = 0;
		showMenu($currentMenu);
	}
	//execute hideAll() BUT with delay

	

	
});  // END DOC READY FUNC


$(window).resize(hideAnimations);

$(window).ready(function () {

	$('body').fadeIn(100);
	

});

var waitForFinalEvent = (function () {
  var timers = {};
  return function (callback, ms, uniqueId) {
    if (!uniqueId) {
      uniqueId = "Don't call this twice without a uniqueId";
    }
    if (timers[uniqueId]) {
      clearTimeout (timers[uniqueId]);
    }
    timers[uniqueId] = setTimeout(callback, ms);
  };
})();

function hideAnimations() {
	//curtain is drawn, hide all of that stuff
	$('#swiffyGraphicWhyApproach').hide();
	$('#swiffyGraphicWhyStats').hide();
	$('#swiffyGraphicWhyWhyitworks').hide();
	$('#swiffyGraphicWhyPowerofhabit').hide();
	$('#swiffyGraphicWhyTailoredrewardmodels').hide();
	$('#swiffyGraphicWhyOurplatform').hide();
	$('#swiffyGraphicWhyStats').hide();
	

	waitForFinalEvent(function(){
		
		//let's see it again!
		$('#swiffyGraphicWhyApproach').fadeIn(100);
		$('#swiffyGraphicWhyStats').fadeIn(100);
		$('#swiffyGraphicWhyWhyitworks').fadeIn(100);
		$('#swiffyGraphicWhyPowerofhabit').fadeIn(100);
		$('#swiffyGraphicWhyTailoredrewardmodels').fadeIn(100);
		$('#swiffyGraphicWhyOurplatform').fadeIn(100);
		$('#swiffyGraphicWhyStats').fadeIn(100);
		
	}, 50, "slider resize");
	
	if ($('#swiffyGraphicWhyApproach').height()== 0){waitForFinalEvent}

};

