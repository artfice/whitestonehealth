// Constants
var FULLEXPERIENCEWIDTH = 640;

// Global Variables
var isIE8 = false;
var isiPad = navigator.userAgent.match(/iPad/i) != null;
var platformFBTimeout = -1;
var powerFBTimeout = -1;

// Functions

if (!window.console) window.console = {};
if (!window.console.log) window.console.log = function () { };

function checkIsIE8()
{
	if ($('html').hasClass('ie8'))
	{
		isIE8 = true;
	}
}

function isFullExperience() {
	return ($(window).width() > FULLEXPERIENCEWIDTH);
}

function scaleToRectangle(obSize, tgtSize)
{
	var tgtW = tgtSize.w;
	var tgtH = tgtSize.h;
	var tgtScale = 1.0;

	var maxRatio = tgtW / tgtH;
	var curRatio = obSize.w / obSize.h;

	if (maxRatio > curRatio)
	{
		tgtScale = tgtH / obSize.h;
	}
	else if (maxRatio < curRatio)
	{
		tgtScale = tgtW / obSize.w;
	}

	var newWidth = Math.floor(obSize.w * tgtScale);
	var newHeight = Math.floor(obSize.h * tgtScale);

	return { w: newWidth, h: newHeight };
}

function fitOverRectangle(obSize, tgtSize)
{
	var tgtW = tgtSize.w;
	var tgtH = tgtSize.h;
	var tgtScale = 1.0;

	var maxRatio = tgtW / tgtH;
	var curRatio = obSize.w / obSize.h;

	if (maxRatio < curRatio)
	{
		tgtScale = tgtH / obSize.h;
	}
	else if (maxRatio > curRatio)
	{
		tgtScale = tgtW / obSize.w;
	}

	var newWidth = Math.ceil(obSize.w * tgtScale);
	var newHeight = Math.ceil(obSize.h * tgtScale);

	return { w: newWidth, h: newHeight };
}

//******************************************************************************
// Why
//******************************************************************************

	var scaledPercent = 1;
	var swiffyStats;
	var swiffyWhyitworks;
	var swiffyPowerofhabit;
	var swiffyOurplatform;
	var lastScrollTop = 0;
	var lastWindowWidth = -1;
	var lastWindowHeight = -1;
	var powerScale = 1;
	var platformScale = 1;
	var popupHotCircle = { r: 0, x: 0, y: 0 };
	var popHot = null;
	var lastPopupType = '';

	var arSectionNames = ['wrbhb-approach', 'wrbhb-stats', 'wrbhb-whyitworks',
		'wrbhb-powerofhabit', 'wrbhb-tailoredrewardmodels', 'wrbhb-ourplatform',
		'wrbhb-focusonresults', 'results-stories'];

	var arSwiffys = [];

	function animateInGraphic(ob) {

		if(typeof ob != 'undefined') {
			if (isIE8)
			{
				ob.showHide('show');
			}
			else
			{
				ob.setFlashVars('myresponse=show');
			}
		}
	}

	function animateOutGraphic(ob) {

		if(typeof ob != 'undefined') {
			if (isIE8)
			{
				ob.showHide('hide');
			}
			else
			{
				ob.setFlashVars('myresponse=hide');
			}
		}
	}

	// Tool Tips

	function handleWhyMouseMove(event) {

		if (popHot != null)
		{
			var px = event.pageX;
			var py = event.pageY;

			var isInCircle = false;
			var isInRect = false;

			if (px >= popHot.rx && px <= popHot.rx + popHot.rw
				&& py >= popHot.ry && py <= popHot.ry + popHot.rh)
			{
				isInRect = true;

			}

			var dx = px - popHot.cx;
			var dy = py - popHot.cy;
			var dd = Math.sqrt(dx * dx + dy * dy);

			if (dd < popHot.cr)
			{
				isInCircle = true;
			}

			if (!(isInCircle || isInRect))
			{
				popHot = null;
				$('#whyRBHttip').css('left', '-999px').css('top', '0px');
				$('#vennttip').css('left', '-999px').css('top', '0px');
				swiffyOurplatform.setFlashVars('myresponse=reset');
				swiffyPowerofhabit.setFlashVars('myresponse=reset');
				lastPopupType = '';
			}
		}
	}


	function positionVennPopup() {
		var px = popupHotCircle.x;
		var py = popupHotCircle.y;

		var ttid = '#tt' + lastPopupType;
		var tH = $(ttid + ' .ttheader').html();
		var tB = $(ttid + ' .ttbody').html();
		var tA = $(ttid + ' .ttlink').html();

		$('#vennttipH').html(tH);
		$('#vennttipB').html(tB);
	/* 	$('#vennttipA').html(tA); */

		var divTop = $('#whyRBH').position().top;
		var elH = $('#vennttipBody').outerHeight();
		var offX = $('#swiffyGraphicWhyPowerofhabit svg').offset().left + 40 + px * platformScale;
		var offY = $('#swiffyGraphicWhyPowerofhabit svg').offset().top - elH/2 - divTop + py * platformScale;
		$('#vennttip').show();
		$('#vennttip').css('left', offX).css('top', offY);
		$('#vennttipArrow').css('left', '-34px').css('top', ((elH - 48)/2) + 'px');

		// Construct hotspot
		popHot = {};
		popHot.rx = $('#vennttipBody').offset().left - 40;
		popHot.ry = $('#vennttipBody').offset().top;
		popHot.rw = $('#vennttipBody').outerWidth() + 40;
		popHot.rh = $('#vennttipBody').outerHeight();
		popHot.cx = $('#swiffyGraphicWhyPowerofhabit svg').offset().left + px * platformScale;
		popHot.cy = $('#swiffyGraphicWhyPowerofhabit svg').offset().top + py * platformScale;
		popHot.cr = popupHotCircle.r;
	}


	function showPowerPopup(venntype, mousex, mousey)
	{
		if (venntype != lastPopupType)
		{
			lastPopupType = venntype;

			if (isFullExperience())
			{


				switch (venntype)
				{
					case 'behavior':
						popupHotCircle = { r: 200, x: 250, y: 135 };
						positionVennPopup();
						break;
					case 'enabling':
						popupHotCircle = { r: 200, x: 450, y: 135 };
						positionVennPopup();
						break;
					case 'consumer':
						popupHotCircle = { r: 200, x: 300, y: 300 };
						positionVennPopup();
						break;
					default:
						break;
				}
			}
			else
			{
				clearTimeout(powerFBTimeout);
				powerFBTimeout = setTimeout('showPowerFB()', 500);
			}
		}
	}

	function showPowerFB()
	{
		jQuery.fancybox.open({
			href: '#tt' + lastPopupType,
				helpers: {
				overlay: {
					locked: false
				}
			}
		});
	}

	function positionPlatformPopup() {
		var px = popupHotCircle.x;
		var py = popupHotCircle.y;

		var ttid = '#tt' + lastPopupType;
		var tH = $(ttid + ' .ttheader').html();
		var tB = $(ttid + ' .ttbody').html();
		var tA = $(ttid + ' .ttlink').html();

		$('#whyRBHttipH').html(tH);
		$('#whyRBHttipB').html(tB);
		$('#whyRBHttipA').html(tA);

		var divTop = $('#whyRBH').position().top;
		var elH = $('#whyRBHttipBody').outerHeight();
		var offX = $('#swiffyGraphicWhyOurplatform svg').offset().left + 40 + px * platformScale;
		var offY = $('#swiffyGraphicWhyOurplatform svg').offset().top - elH/2 - divTop + py * platformScale -30;
		$('#whyRBHttip').show();
		$('#whyRBHttip').css('left', offX).css('top', offY);
		$('#whyRBHttipArrow').css('left', '-34px').css('top', ((elH - 48)/2) + 'px');

		// Construct hotspot
		popHot = {};
		popHot.rx = $('#whyRBHttipBody').offset().left - 40;
		popHot.ry = $('#whyRBHttipBody').offset().top;
		popHot.rw = $('#whyRBHttipBody').outerWidth() + 40;
		popHot.rh = $('#whyRBHttipBody').outerHeight();
		popHot.cx = $('#swiffyGraphicWhyOurplatform svg').offset().left + px * platformScale;
		popHot.cy = $('#swiffyGraphicWhyOurplatform svg').offset().top + py * platformScale;
		popHot.cr = popupHotCircle.r * platformScale;
	}

	function showPlatformPopup(type, mousex, mousey)
	{
		if (type != lastPopupType)
		{
			lastPopupType = type;

			if (isFullExperience())
			{
				switch (type)
				{
					case 'assessments':
						popupHotCircle = { r: 37, x: 110, y: 397 };
						positionPlatformPopup();
						break;
					case 'biometrics':
						popupHotCircle = { r: 37, x: 57, y: 295 };
						positionPlatformPopup();
						break;
					case 'advising':
						popupHotCircle = { r: 37, x: 65, y: 182 };
						positionPlatformPopup();
						break;
					case 'multimodal':
						popupHotCircle = { r: 37, x: 136, y: 89 };
						positionPlatformPopup();
						break;
					case 'telephone':
						popupHotCircle = { r: 37, x: 257, y: 53 };
						positionPlatformPopup();
						break;
					case 'consumerprograms':
						popupHotCircle = { r: 37, x: 373, y: 88 };
						positionPlatformPopup();
						break;
					case 'deviceintegration':
						popupHotCircle = { r: 37, x: 447, y: 183 };
						positionPlatformPopup();
						break;
					case 'challenges':
						popupHotCircle = { r: 37, x: 450, y: 295 };
						positionPlatformPopup();
						break;
					case 'transparency':
						popupHotCircle = { r: 37, x: 398, y: 397 };
						positionPlatformPopup();
						break;
					default:
						break;
				}
			}
			else
			{
				clearTimeout(platformFBTimeout);
				platformFBTimeout = setTimeout('showPlatformFB()', 500);
			}
		}
	}

	function showPlatformFB() {
		jQuery.fancybox.open({
			href: '#tt' + lastPopupType,
				helpers: {
				overlay: {
					locked: false
				}
			}
		});
	}

	// Scroll handling

	function isScrolledIntoView(elem)
	{
		var docViewTop = $(window).scrollTop();
		var docViewBottom = docViewTop + $(window).height();

		var elemTop = $(elem).offset().top;
		var elemBottom = elemTop + $(elem).height();

		return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
	}

	// HANDLERS
	function isInView(elem)
	{
		var docViewTop = $(window).scrollTop();
		var docViewBottom = docViewTop + $(window).height();

		var elemTop = $(elem).offset().top;
		var elemBottom = elemTop + $(elem).height();

		return ((elemBottom <= docViewBottom) || (elemTop >= docViewTop));
	}

	function shouldBeVisible(elem)
	{
		var wH = $(window).height();
		var scrollTop = $(window).scrollTop();
		var animPoint = scrollTop + wH * 0.6666;

		var elemTop = $(elem).offset().top;
		var elemBottom = elemTop + $(elem).height();

		if (elemTop < animPoint)
		{
			return true;
		}

		if (isiPad)
		{
			padding = 200;
		}
		else
		{
			padding = 10;
		}

		if (elemBottom <= scrollTop + wH + padding)
		{
			return true;
		}

		if (isiPad && scrollTop >= $(document).height() - wH - 500)
		{
			return true;
		}

		return false;
	}

	function handleWhyScroll(evt)
	{
		var i;

		var textStringTemplate = '#ID# h1, #ID# h2, #ID# h3, #ID# h4, #ID# h5, #ID# p, #ID# ul, #ID# img';

		//if (!isFullExperience()) return;

		var scrollTop = $(window).scrollTop();
		var dScroll = scrollTop - lastScrollTop;
		var f;

		for (i = 0; i < arSectionNames.length; i++)	{
			var id = '.' + arSectionNames[i];
			var textString = textStringTemplate.replace(/\#ID\#/g, id);

			if (shouldBeVisible(id)) {
				if (!$(id).hasClass('sectionVisible'))
				{
					$(id).addClass('sectionVisible');

					$(textString).animate({
						opacity: 1
					}, 1000);

					if (arSwiffys.length > i)
					{
						if (arSwiffys[i] != null)
						{
							animateInGraphic(arSwiffys[i]);
						}
					}
				}

				maxVisible = i;
			}
			else {
				if ($(id).hasClass('sectionVisible'))
				{
					$(id).removeClass('sectionVisible')

					$(textString).animate({
						opacity: 0
					}, 1000);

					if (arSwiffys.length > i)
					{
						if (arSwiffys[i] != null)
						{
							animateOutGraphic(arSwiffys[i]);
						}
					}
				}
			}
		}
	}

	function handleWhyResize() {

		if ($(window).width() != lastWindowWidth || $(window).height() != lastWindowHeight)
		{
			lastWindowWidth = $(window).width();
			lastWindowHeight = $(window).height();

			resizeStatsSVG();
			resizeWhyitworksSVG();
			resizePowerofhabitSVG();
			resizeOutplatformSVG();
		}

		setTimeout('fixTransform()', 1);
	}


	function resizeStatsSVG() {
		var w = $('#swiffyGraphicWhyStats').width();
		var h = $('#swiffyGraphicWhyStats').height();
		var tgtSize = { w: w, h: h };
		var scaledSize = scaleToRectangle({ w: 493, h: 371}, tgtSize);
		scaledPercent = scaledSize.w/493;

		$('#swiffyGraphicWhyStats div').css('width', scaledSize.w + 'px');
		$('#swiffyGraphicWhyStats div').css('height', scaledSize.h + 'px');
		if (isFullExperience()) {
			$('#swiffyGraphicWhyStats div').css('top', '-40px');
		}
		else
		{
			$('#swiffyGraphicWhyStats div').css('top', '0px');
		}

		$('#swiffyGraphicWhyStats div').css('left', ((tgtSize.w - scaledSize.w)/2) + 'px');
		$('#swiffyGraphicWhyStats svg').css('width', scaledSize.w + 'px');
		$('#swiffyGraphicWhyStats svg').css('height', scaledSize.h + 'px');
	}

	function resizeWhyitworksSVG() {

        var w = $('#swiffyGraphicWhyWhyitworks').width();
		var h = $('#swiffyGraphicWhyWhyitworks').height();
		var tgtSize = { w: w, h: h };
		var scaledSize = scaleToRectangle({ w: 352, h: 271}, tgtSize);
		scaledPercent = scaledSize.w/352;

		$('#swiffyGraphicWhyWhyitworks div').css('width', scaledSize.w + 'px');
		$('#swiffyGraphicWhyWhyitworks div').css('height', scaledSize.h + 'px');
		$('#swiffyGraphicWhyWhyitworks div').css('top', ((tgtSize.h - scaledSize.h)/2) + 'px');
		$('#swiffyGraphicWhyWhyitworks div').css('left', ((tgtSize.w - scaledSize.w)/2) + 'px');
		$('#swiffyGraphicWhyWhyitworks svg').css('width', scaledSize.w + 'px');
		$('#swiffyGraphicWhyWhyitworks svg').css('height', scaledSize.h + 'px');

	}

	function resizePowerofhabitSVG() {
		var w = $('#swiffyGraphicWhyPowerofhabit').width();
		var h = 999; //$('#swiffyGraphicWhyPowerofhabit').height();
		var tgtSize = { w: w, h: h };
		var scaledSize = scaleToRectangle({ w: 457, h: 420 }, tgtSize);
		powerScale = scaledSize.w/457;

		$('#swiffyGraphicWhyPowerofhabit').css('height', scaledSize.h + 'px');
		$('#swiffyGraphicWhyPowerofhabit div').css('width', scaledSize.w + 'px');
		$('#swiffyGraphicWhyPowerofhabit div').css('height', scaledSize.h + 'px');
		$('#swiffyGraphicWhyPowerofhabit div').css('top', '-40px');
		$('#swiffyGraphicWhyPowerofhabit div').css('left', ((tgtSize.w - scaledSize.w)/2) + 'px');
		$('#swiffyGraphicWhyPowerofhabit svg').css('width', scaledSize.w + 'px');
		$('#swiffyGraphicWhyPowerofhabit svg').css('height', scaledSize.h + 'px');
	}


	function resizeOutplatformSVG() {
		var w = $('#swiffyGraphicWhyOurplatform').width();
		var h = $('#swiffyGraphicWhyPowerofhabit').height();
		var tgtSize = { w: w, h: h };
		var scaledSize = scaleToRectangle({ w: 515, h: 515 }, tgtSize);
		platformScale = scaledSize.w/515;

		$('#swiffyGraphicWhyOurplatform').css('height', scaledSize.h + 'px');
		$('#swiffyGraphicWhyOurplatform div').css('width', scaledSize.w + 'px');
		$('#swiffyGraphicWhyOurplatform div').css('height', scaledSize.h + 'px');

		if (isFullExperience())
		{
			$('#swiffyGraphicWhyOurplatform div').css('top', '-30px');
		}
		else
		{
			$('#swiffyGraphicWhyOurplatform div').css('top', '0px');
		}

		$('#swiffyGraphicWhyOurplatform div').css('left', ((tgtSize.w - scaledSize.w)/2) + 'px');
		$('#swiffyGraphicWhyOurplatform svg').css('width', scaledSize.w + 'px');
		$('#swiffyGraphicWhyOurplatform svg').css('height', scaledSize.h + 'px');
	}

	function fixTransform() {
		handleWhyScroll(null); // in case the visibilities have changed
		fixTransformById('swiffyGraphicWhyStats');
		fixTransformById('swiffyGraphicWhyWhyitworks');
		fixTransformById('swiffyGraphicWhyPowerofhabit');
		fixTransformById('swiffyGraphicWhyOurplatform');
	}

	function fixTransformById(id)
	{
		if (typeof $('#' + id + ' svg > g').attr('transform') != 'undefined')
		{
			var arBits = $('#' + id + ' svg > g').attr('transform').split(' ');

			if (arBits[4] != '0' || arBits[5] != '0)')
			{
				arBits[4] = '0';
				arBits[5] = '0)';
				var newTransform = arBits.join(' ');
				$('#' + id + ' svg > g').attr('transform', newTransform);
			}
		}
	}

	function initWhySwiffys()
	{

		// Stats

		$('#swiffyGraphicWhyStats').addClass('swiffied');
		$('#swiffyGraphicWhyStats table').hide();
		swiffyStats = new swiffy.Stage(document.getElementById('swiffyGraphicWhyStats'), swiffyStatsData);
		swiffyStats.start();
		addSwiffyResizeInfo('swiffyGraphicWhyStats', 493, 371);

		arSwiffys.push(swiffyStats);

		// Why It Works

		$('#swiffyGraphicWhyWhyitworks').addClass('swiffied');
		$('#swiffyGraphicWhyWhyitworks table').hide();
		swiffyWhyitworks = new swiffy.Stage(document.getElementById('swiffyGraphicWhyWhyitworks'), swiffyWhyitworksData);
		swiffyWhyitworks.start();
		addSwiffyResizeInfo('swiffyGraphicWhyWhyitworks', 586, 271);

		arSwiffys.push(swiffyWhyitworks);

		// Power of Habit

		$('#swiffyGraphicWhyPowerofhabit').addClass('swiffied').css('marginBottom', '-24px');
		$('#swiffyGraphicWhyPowerofhabit img').hide();
		swiffyPowerofhabit = new swiffy.Stage(document.getElementById('swiffyGraphicWhyPowerofhabit'), swiffyPowerofhabitData);
		swiffyPowerofhabit.start();
		addSwiffyResizeInfo('swiffyGraphicWhyPowerofhabit', 457, 420);

		arSwiffys.push(swiffyPowerofhabit);

		// Our Platform

		$('#swiffyGraphicWhyOurplatform').addClass('swiffied');
		$('#swiffyGraphicWhyOurplatform img').hide();
		swiffyOurplatform = new swiffy.Stage(document.getElementById('swiffyGraphicWhyOurplatform'), swiffyOurplatformData);
		swiffyOurplatform.start();
		addSwiffyResizeInfo('swiffyGraphicWhyOurplatform', 515, 515);

		arSwiffys.push(swiffyOurplatform);
	}

	function addSwiffyResizeInfo(id, width, height) {
		$('#' + id + ' svg').attr('viewBox', '0 0 ' + width + ' ' + height)
			.attr('preserveAspectRatio', 'xMinYMin meet')
			.attr('class', 'svg-content');
	}

	$(document).ready(function()
	{
		checkIsIE8();

		if (isIE8)
		{

		}
		else
		{
			initWhySwiffys();

			setInterval('handleWhyResize()', 300);

			handleWhyScroll(null);

			// Waypointing
			$(window).scroll(handleWhyScroll);

			// Tooltip tracking

			$(document).mousemove(handleWhyMouseMove);

			$(document).on('touchstart', function () {
			handleWhyMouseMove();
			});

		}
	});

/* 	$('.whyRBHbladeinner').on('touchstart', function(){ handleWhyMouseMove(e); }); */

	$(window).resize(function() {

		handleWhyResize();
	});

	 $(window).load(function() {

		handleWhyResize();
	});
//******************************************************************************
// Leadership
//******************************************************************************

	var isLeaderTwoCol = false;
	var aboutLdrItems = [];

	function putLeaderItemsOneCol()
	{
		var i;

		for (i = 0; i < aboutLdrItems.length; i++)
		{
			$('#aboutLeaderColumn0').append(aboutLdrItems[i]);
		}

		if (isIE8)
		{
			$('#aboutLeaderColumn0').addClass('aboutLeaderColumn0IE8');
			$('#aboutLeaderColumn1').addClass('aboutLeaderColumn1IE8');
		}

		isLeaderTwoCol = false;
	}

	function putLeaderItemsTwoCol()
	{
		var i;

		for (i = 0; i < aboutLdrItems.length; i++)
		{
			if (i % 2 == 0)
			{
				$('#aboutLeaderColumn0').append(aboutLdrItems[i]);
			}
			else
			{
				$('#aboutLeaderColumn1').append(aboutLdrItems[i]);
			}
		}

		if (isIE8)
		{
			$('#aboutLeaderColumn0').removeClass('aboutLeaderColumn0IE8');
			$('#aboutLeaderColumn1').removeClass('aboutLeaderColumn1IE8');
		}

		isLeaderTwoCol = true;
	}

	function handleLeaderExpandClick(e) {
		e.preventDefault();

		var i;

		var parent = $(this).parent().parent();
		var closedItemHeight = 0;
		var selectedItemIndex = 0;
		var firstY = 0;
		var closedItemHeight = 0;
		var targetY;

		$('.aboutLeaderItem').each(function(index) {
			if (!$(this).hasClass('selected'))
			{
				closedItemHeight = $(this).height();
			}
		});

		for (i = 0; i < aboutLdrItems.length; i++)
		{
			if (i == 0)
			{
				var offset = aboutLdrItems[i].offset();
				firstY = offset.top;
			}

			if (aboutLdrItems[i].is(parent))
			{
				selectedItemIndex = i;
				break;
			}
		}

		$('.aboutLeaderItem').each(function(index) {
			if (!$(this).is(parent))
			{
				if ($(this).hasClass('selected'))
				{
					$(this).removeClass('selected');
					$(this).find('.aboutLeaderCTA a').removeClass('selected');
					$(this).find('.aboutLeaderBio').stop().animate({
						height: 0
					}, 500);
				}
				else
				{
					closedItemHeight = $(this).height() + 25;
				}
			}
		});

		$('.aboutLeaderCTA a').removeClass('selected');

		if (parent.hasClass('selected'))
		{
			parent.removeClass('selected');
			$(this).removeClass('selected');

			parent.find('.aboutLeaderBio').stop().animate({
				height: 0
			}, 500);
		}
		else
		{
			parent.addClass('selected');
			$(this).addClass('selected');

			var newHeight = parent.find('.aboutLeaderBioInner').height();

			parent.find('.aboutLeaderBio').stop().animate({
				height: newHeight
			}, 500);
		}

		if (isLeaderTwoCol)
		{
			targetY = firstY + closedItemHeight * Math.floor(selectedItemIndex/2);
			$(window).scrollTo(targetY + 'px', 500, { delay: 300 });
		}
		else
		{
			targetY = firstY + closedItemHeight * (selectedItemIndex);
			$(window).scrollTo(targetY + 'px', 500, { delay: 300 });
		}

		return false;
	}

	function handleLeaderResize() {

		if ($(window).width() > 640)
		{
			putLeaderItemsTwoCol();
		}
		else
		{
			putLeaderItemsOneCol();
		}
	}

	function initLeadership() {
		// Gather up the leader items
		$('.aboutLeaderItem').each(function(index) {
			aboutLdrItems.push($(this));
		});

		$('.aboutLeaderCTA a').bind('click', handleLeaderExpandClick);

		handleLeaderResize();

		$(window).resize(function() {
			handleLeaderResize();
		});
	}

	$(document).ready(function()
	{
		checkIsIE8();

		initLeadership();
	});
